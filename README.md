# Authors
- Clément Legrand, clement.legrand4.etu@univ-lille.fr
- Laetitia Jourdan, laetitia.jourdan@univ-lille.fr
- Marie-Eléonore Kessaci, marie-eleonore.kessaci@univ-lille.fr
- Diego Cattaruzza, diego.cattaruzza@centralelille.fr

# Description
This project contains the following elements:
- the detailed results obtained on each instance for each experiment (file Additional_results.pdf)
- the source code used to run the experiments, with instructions for its execution (archive source_code.zip)
- the best pareto fronts obtained considering all our experiments (archive bestFronts.zip)
